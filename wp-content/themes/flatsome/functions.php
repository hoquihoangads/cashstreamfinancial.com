<?php
/**
 * Flatsome functions and definitions
 *
 * @package flatsome
 */

require get_template_directory() . '/inc/init.php';

/**
 * Note: It's not recommended to add any custom code here. Please use a child theme so that your customizations aren't lost during updates.
 * Learn more here: http://codex.wordpress.org/Child_Themes
 */
function xw_custom_billing_fields( $fields = array() ) {

    $fields['billing_first_name']['label'] = 'Full name';
	$fields['billing_first_name']['placeholder'] = 'Full name';
    $fields['billing_first_name']['class'] = ['form-row-wide'];

    $fields['billing_country']['class'] = ['form-row-wide'];

    $fields['billing_city']['class'] = ['form-row-first'];

    $fields['billing_state']['class'] = ['form-row-last'];

    $fields['billing_postcode']['class'] = ['form-row-wide'];

    $fields['billing_email']['priority'] = 0;



    unset($fields['billing_last_name']);
    unset($fields['billing_company']);

    return $fields;

}
add_filter('woocommerce_billing_fields','xw_custom_billing_fields');
function xw_custom_shipping_fields($fields = array()){

    $fields['shipping_first_name']['label'] = 'Full name';
    $fields['shipping_first_name']['class'] = ['form-row-wide'];

    $fields['shipping_country']['class'] = ['form-row-first'];

    $fields['shipping_city']['class'] = ['form-row-first'];

    $fields['shipping_state']['class'] = ['form-row-last'];

    $fields['shipping_postcode']['priority'] = 45;
    $fields['shipping_postcode']['class'] = ['form-row-last'];

    $fields['shipping_email']['priority'] = 10;

    unset($fields['shipping_last_name']);
    unset($fields['shipping_company']);
    unset($fields['shipping_phone']);

    return $fields;
}
add_filter('woocommerce_shipping_fields', 'xw_custom_shipping_fields', 10, 2);